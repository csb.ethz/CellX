%
% CellX generate release script
%
% Requires: java, zip
%

clear;

% clean the default path in matlab on the linux terminal servers
%rmpath(userpath);

currentFolder = fileparts(mfilename('fullpath'))

% path to CellXConfiguration class to get the current version number
mclassesAbsPath = cd(cd([currentFolder filesep '..' filesep 'mclasses']))
addpath(mclassesAbsPath);

mscriptsAbsPath = cd(cd([currentFolder filesep '..' filesep 'mscripts']))
addpath(mscriptsAbsPath);

mfunctionsAbsPath = cd(cd([currentFolder filesep '..' filesep 'mfunctions']))
addpath(mfunctionsAbsPath);

mexAbsPath = cd(cd([currentFolder filesep '..' filesep 'mex']))
addpath(mexAbsPath);

mexMaxflowAbsPath = cd(cd([currentFolder filesep '..' filesep 'mex' filesep 'maxflow']))
addpath(mexMaxflowAbsPath);
%%


%%
os = java.lang.System.getProperty('os.name');
os = os.replaceAll(' ', '_');

if( os.startsWith('Mac') )
    version = java.lang.System.getProperty('os.version');
    os = concat(os, version);
end

if( os.startsWith('Windows') )
    %java = '"C:\Program Files (x86)\Java\jre7\bin\java.exe"';
    [e, guiVersion] = system(['java -jar ..' filesep '..' filesep 'gui' filesep 'CellXGui.jar -version']);
    guiVersion = strtrim(guiVersion);
else
    [e, guiVersion] = system(['java -jar ..' filesep '..' filesep 'gui' filesep 'CellXGui.jar -version']);
    guiVersion = strtrim(guiVersion);
end
releaseName = sprintf('%s_g%s_c%d.%02d', char(os), guiVersion, CellXConfiguration.version, CellXConfiguration.release);


fprintf('Removing previous files ...\n');
try
    rmdir(releaseName, 's');
catch e
end

try
    delete([releaseName '.zip']);
catch e
end

fprintf('Building release %s ...\n', releaseName);

makeMex

if( os.startsWith('Mac' ) )
    dp = [matlabroot filesep 'bin' filesep 'deploytool -build batchCellXMac.prj']
elseif( os.startsWith('Windows' ) )
    dp = ['"' matlabroot filesep 'bin' filesep 'deploytool.bat" -build batchCellXWin.prj']
else
    dp = [matlabroot filesep 'bin' filesep 'deploytool -build batchCellX.prj']
end
[e,m] = system(dp);


if( e==1 )    
    fprintf('%s\n', m);
    error('deploytool failed');
end

mkdir([releaseName filesep 'lib']);

if( os.startsWith('Mac' ) )
   bindir = ['batchCellXMac' filesep 'distrib' filesep 'batchCellX*'];  
   copyfile(bindir, releaseName);
elseif( os.startsWith('Windows') )
   bindir = ['batchCellXWin' filesep 'distrib' filesep 'batchCellXWin.exe'];
   exe = [releaseName filesep 'CellX.exe'];
   copyfile(bindir, exe);
else
   bindir = ['batchCellX' filesep 'distrib' filesep 'batchCellX*'];
   copyfile(bindir, releaseName);
end


if( os.startsWith('Mac') )
    system(['cp CellX.sh.mac ' releaseName '/CellX.sh']);
elseif( os.startsWith('Windows') )
    % do nothing
else
    system(['cp CellX.sh.linux ' releaseName '/CellX.sh']);
end

copyfile('LICENSE.txt', releaseName);

copyfile(['..' filesep '..' filesep 'gui' filesep 'CellXGui.jar'], releaseName);

copyfile(['..' filesep '..' filesep 'gui' filesep 'lib' filesep '*.jar'], [releaseName filesep 'lib']);

mkdir([releaseName filesep 'cluster']);
copyfile(['..' filesep '..' filesep 'cluster' filesep 'README.txt' ], [releaseName filesep 'cluster']);
copyfile(['..' filesep '..' filesep 'cluster' filesep 'submit.pl' ], [releaseName filesep 'cluster']);

if( os.startsWith('Mac'))
    % mcr = '/Applications/MATLAB_R2016a.app/toolbox/compiler/deploy/maci64/MCRInstaller.dmg';
    mcr = '/Applications/MATLAB_R2016a.app/toolbox/compiler/deploy/maci64/MCRInstaller.zip';
elseif( os.startsWith('Windows' ) )
    % mcr = 'C:\Program Files\MATLAB\R2016a\toolbox\compiler\deploy\win32\MCRInstaller.exe';
    mcr = 'C:\Program Files (x86)\MATLAB\R2016a\toolbox\compiler\deploy\win64\MCRInstaller.exe';
else
    mcr = '/net/bs-gridsw/sw-repo/bsse/MATLAB_R2016a/toolbox/compiler/deploy/glnxa64/MCRInstaller.bin';
end

% copyfile(mcr, releaseName);

if( ~os.startsWith('Windows') )
    system(['zip -r ' releaseName '.zip ' releaseName '/*']);
end

