function Idx = get_TextIndicesOnImage(Image,String,Position,Font,FontSize, Idx)
%get_TextIndicesOnImage Outputs image indices of a rasterized version of 
% text from a string at a specified position. This function uses a 
% modified RasterizeText function written by Daniel Warren and 
% get_TextToImage by Siyi Deng.
% Input:
%   Image:             Image [NxM] matrix where the text string
%                                  should be inserted. Note: Can also be
%                                  just an emtpy matrix with the same size
%                                  as the image, as currently the text
%                                  insertion onto the image is not
%                                  implemented in this function, only the
%                                  respective pixel indices are returned.
%   String:            Text [String] that should be added to the image
%   Position:          Position [double, 2x1] with x and y coordinates to
%                                   place the string onto the image
%   Font:              Font Name[String]
%   FontSize:          Font Size [double]
%   Idx:               String indices [nx1, array]. Pixel indices of the
%                                   text placed onto the image at the given 
%                                   position. 
%
% Output:                     
%   Idx:               String indices [nx1, array]. Note: This array can be 
%                                   grown in a loop if multiple text
%                                   strings need to be placed on an image.
% Example:
%   Image = rand([100,100,3]);
%   Idx = get_TextIndicesOnImage(Image,'Hello World',[50,50],'Arial',8,
%   []);
%   % Label the image with the text only in the green channel
%   Image(Idx+(size(Image,1)^2)) = 1;
%   % If the image is in greyscale only use:
%   Image(Idx) = 1;
%   figure; imshow(Image); 
%
% Andreas P. Cuny, 10-02-2017;

% Check input
if ~exist('Image','var') || isempty(Image)
    error('No image specified');
end
if ~exist('String','var')
    error('No string specified.');
end
if ~exist('Position','var')
    error('No position specified');
end
if ~exist('Font','var')
    Font = 'Arial';
end
if ~exist('FontSize','var')
    FontSize = 10;
end

% Get rasterized text
TextMask = RasterizeText(String,Font,FontSize);
TextMaskCenterOffset = round(size(TextMask,2)/2);
% Overlay rasterized text at correct position in image and get these pixel
% indices. The array Idx is grown at each evaluation of this function.
[x, y]= find(TextMask > 0 );
CurrentIdx = (size(Image,1)*(y+Position(2)-y(1)-TextMaskCenterOffset) + (x+Position(1))-x(1));
Idx = [Idx;CurrentIdx];
end