clear; clc; close all;

thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);
addpath('../mscripts');
addpath('../mclasses');
addpath('../mfunctions');
addpath('../mex');
addpath('../mex/maxflow');




%------paths
calibrationfilename = './calibration_workflow.xml';
resultsDir = './workflowResults';
frameNumber = 1;
segImage = '../data/Figure1/Trans_020011.tif';

%--------- produce paper figures
%-------- run segmentation
% get calibration file
config = CellXConfiguration.readXML(calibrationfilename);
config.setDebugLevel(0);
config.check();
% get file set
fileSet = CellXFileSet(frameNumber, segImage);
fileSet.setResultsDirectory(resultsDir);

% run the segmenter
seg = CellXSegmenter(config, fileSet);
seg.run();


seed1 = seg.seeds(1);
seed2 = seg.seeds(2);


seed1Crop = imcrop(seg.imageExtended, seed1.cropRegion);

imgDim = size(seed1Crop)
%imtool(seed1Crop);


fn1 = sprintf('%s_%dx%d.tiff', 'seed1', imgDim(1), imgDim(2));
imwrite(seed1Crop, fn1, 'Compression','none');

seed2Crop = imcrop(seg.imageExtended, seed2.cropRegion);
fn2 = sprintf('%s_%dx%d.tiff', 'seed2', imgDim(1), imgDim(2));
%imtool(seed2Crop);
imwrite(seed2Crop, fn2,'Compression','none');


   