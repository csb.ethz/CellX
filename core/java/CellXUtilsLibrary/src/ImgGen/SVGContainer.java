/*
 * SVGContainer.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXUtilsLibrary
 *
 * CellXUtilsLibraryis free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * CellXUtilsLibrary is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package ImgGen;

import org.apache.batik.svggen.SVGGraphics2D;
import org.w3c.dom.svg.SVGDocument;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class SVGContainer {
    
    private final SVGGraphics2D g2;
    private final SVGDocument doc;

    public SVGContainer(SVGGraphics2D g2, SVGDocument doc) {
        this.g2 = g2;
        this.doc = doc;
    }

    public SVGDocument getDoc() {
        return doc;
    }

    public SVGGraphics2D getG2() {
        return g2;
    }
    
    
    
    
}
