/*
 * BezierSpline.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXUtilsLibrary
 *
 * CellXUtilsLibraryis free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * CellXUtilsLibrary is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package ImgGen;

import java.awt.Shape;
import java.awt.geom.GeneralPath;
import java.util.List;
import math.geom2d.Point2D;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class BezierSpline {

    public enum EndMode {

        INTERPOLATE_CIRCULAR_AND_CLOSED,
        INTERPOLATE_CIRCULAR_AND_OPEN,
        SMOOTH_START_AND_END
    }
    private final GeneralPath path;
    private final double[][] knots;
    private final EndMode mode;
    private final double edgeSmoothing;

    public BezierSpline(double[][] knots, EndMode mode, double edgeSmoothing) {
        this.knots = knots;
        this.mode = mode;
        this.edgeSmoothing = edgeSmoothing;
        this.path = new GeneralPath();
        init();
    }

    private void init() {
        if (knots.length < 3) {
            throw new Error("Bezier spline requires at least 3 points");
        }
        if (mode.equals(EndMode.SMOOTH_START_AND_END)) {
            path.moveTo(knots[0][0], knots[0][1]);
            final double startControls[] = computeStartControlPoints(knots[0][0], knots[0][1], knots[1][0], knots[1][1], knots[2][0], knots[2][1], edgeSmoothing);
            path.curveTo(startControls[0], startControls[1], startControls[2], startControls[3], knots[1][0], knots[1][1]);
            for (int i = 0; i < knots.length - 3; ++i) {
                final double controls[] = computeControlPoints(
                        knots[i][0], knots[i][1],
                        knots[i + 1][0], knots[i + 1][1],
                        knots[i + 2][0], knots[i + 2][1],
                        knots[i + 3][0], knots[i + 3][1],
                        edgeSmoothing);
                path.curveTo(controls[0], controls[1], controls[2], controls[3], knots[i + 2][0], knots[i + 2][1]);
            }
            final int e = knots.length - 3;
            final double endControls[] = computeEndControlPoints(knots[e][0], knots[e][1], knots[e + 1][0], knots[e + 1][1], knots[e + 2][0], knots[e + 2][1], edgeSmoothing);
            path.curveTo(endControls[0], endControls[1], endControls[2], endControls[3], knots[e + 2][0], knots[e + 2][1]);
        } else if (mode.equals(EndMode.INTERPOLATE_CIRCULAR_AND_CLOSED) || mode.equals(EndMode.INTERPOLATE_CIRCULAR_AND_OPEN)) {
            path.moveTo(knots[0][0], knots[0][1]);
            final int n = knots.length;
            final int segments = n + (mode.equals(EndMode.INTERPOLATE_CIRCULAR_AND_OPEN) ? -1 : 0);
            for (int i = 0; i < segments; ++i) {
                final double controls[] = computeControlPoints(
                        knots[mod(i-1,n)][0], knots[mod(i-1,n)][1],
                        knots[i % n][0], knots[i % n][1],
                        knots[(i + 1) % n][0], knots[(i + 1) % n][1],
                        knots[(i + 2) % n][0], knots[(i + 2) % n][1],
                        edgeSmoothing);
                path.curveTo(controls[0], controls[1], controls[2], controls[3], knots[(i+1) % n][0], knots[(i+1) % n][1]);
            }
        }
    }

    public static double[] computeControlPoints(double x0, double y0, double x1, double y1, double x2, double y2, double x3, double y3, double unsmooth) {

        final double len1 = getDistance(x0, y0, x1, y1);
        final double len2 = getDistance(x1, y1, x2, y2);
        final double len3 = getDistance(x2, y2, x3, y3);

        final double ax = 0.5 * (x2 - x0);
        final double ay = 0.5 * (y2 - y0);

        final double al = getLength(ax, ay);

        final double bx = 0.5 * (x3 - x1);
        final double by = 0.5 * (y3 - y1);

        final double bl = getLength(bx, by);

        final double phi1 = unsmooth * len2 / (len1 + len2);

        final double c1x = phi1 * ax + x1;
        final double c1y = phi1 * ay + y1;

        final double phi2 = unsmooth * len2 / (len2 + len3);
        final double c2x = x2 - phi2 * bx;
        final double c2y = y2 - phi2 * by;

        return new double[]{c1x, c1y, c2x, c2y};

    }

    public static double[] computeStartControlPoints(double x0, double y0, double x1, double y1, double x2, double y2, double edgeSmoothing) {

        final double len1 = getDistance(x0, y0, x1, y1);
        final double len2 = getDistance(x1, y1, x2, y2);


        final double ax = 0.5 * (x2 - x0);
        final double ay = 0.5 * (y2 - y0);

        final double al = getLength(ax, ay);

        final double phi1 = edgeSmoothing* len1 / (len1 + len2);

        final double c2x = x1 - phi1 * ax;
        final double c2y = y1 - phi1 * ay;

        final double sx = c2x - x0;
        final double sy = c2y - y0;

        final double c1x = x0 + edgeSmoothing* 0.5 * sx;
        final double c1y = y0 + edgeSmoothing*0.5 * sy;

        return new double[]{c1x, c1y, c2x, c2y};

    }

    private static double[] computeEndControlPoints(double x0, double y0, double x1, double y1, double x2, double y2, double edgeSmoothing) {

        final double len1 = getDistance(x0, y0, x1, y1);
        final double len2 = getDistance(x1, y1, x2, y2);

        final double ax = 0.5 * (x2 - x0);
        final double ay = 0.5 * (y2 - y0);

        final double al = getLength(ax, ay);

        final double phi1 = edgeSmoothing* len2 / (len1 + len2);

        final double c1x = x1 + phi1 * ax;
        final double c1y = y1 + phi1 * ay;

        final double sx = x2 - c1x;
        final double sy = y2 - c1y;

        final double c2x = x2 - edgeSmoothing*0.5 * sx;
        final double c2y = y2 - edgeSmoothing*0.5 * sy;

        return new double[]{c1x, c1y, c2x, c2y};

    }

    public static double getDistance(double x0, double y0, double x1, double y1) {
        return Math.sqrt((x0 - x1) * (x0 - x1) + (y0 - y1) * (y0 - y1));
    }

    public static double getLength(double x, double y) {
        return Math.sqrt(x * x + y * y);
    }

    private GeneralPath getPath() {
        return path;
    }

    public static Shape createBezierSpline(double[][] knots, EndMode mode, double edgeSmoothing) {
        return new BezierSpline(knots, mode, edgeSmoothing).getPath();
    }

    public static GeneralPath createBezierSpline(double[] x, double[] y, EndMode mode, double edgeSmoothing) {
        final int n = x.length;
        double[][] knots = new double[n][];
        for (int i = 0; i < n; ++i) {
            knots[i] = new double[]{x[i], y[i]};
        }
        return new BezierSpline(knots, mode, edgeSmoothing).getPath();
    }
    
    public static GeneralPath createBezierSpline(List<Point2D> knotsList, EndMode mode, double edgeSmoothing, double scale) {
        final int n = knotsList.size();
        double[][] knots = new double[n][];
        for (int i = 0; i < n; ++i) {
            knots[i] = new double[]{knotsList.get(i).getX()*scale, knotsList.get(i).getY()*scale};
        }
        return new BezierSpline(knots, mode, edgeSmoothing).getPath();
    }
    
    
    public static int mod(int i, int n){
        return ((i%n)+8)%n;
    }
    
}
