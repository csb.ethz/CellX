/*
 * Convolutor.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXUtilsLibrary
 *
 * CellXUtilsLibraryis free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * CellXUtilsLibrary is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package ImgGen;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class Convolutor {

    public static double[] convolute(double[] values, double[] signal, int offset) {
        final int n = values.length;
        final double[] ret = new double[n];
        final double[] nv = center(values);
        double conv;

        final int end = n - signal.length + offset + 1;
        for (int i = offset; i < end; ++i) {
            conv = 0.0;
            for (int j = 0; j < signal.length; ++j) {
                conv += nv[i - offset + j] * signal[j];
            }
            ret[i] = conv;
        }
        computeBorderCase(values, signal, offset, ret);
        return ret;
    }

    public static void computeBorderCase(double[] values, double[] signal, int offset, double[] ret) {
        double sigMean = 0.0;
        double d;
        for (int i = 0; i < offset; ++i) {
            final int sigStart = offset - i;
            for (int j = sigStart; j < signal.length; ++j) {
                sigMean += signal[j];
            }
            sigMean /= signal.length - sigStart;
            d = 0.0;
            for (int j = sigStart; j < signal.length; ++j) {
                d += values[j - sigStart] * signal[j];
            }
            ret[i] = d;
        }
    }

    public static double[] center(double values[]) {
        double mean = 0.0;
        for (int i = 0; i < values.length; ++i) {
            mean += values[i];
        }
        mean /= values.length;

        final double[] ret = new double[values.length];
        for (int i = 0; i < values.length; ++i) {
            ret[i] = values[i] - mean;
        }
        return ret;
    }

    public static double[] normalize(double values[]) {

        final double[] mm = getMinMaxValue(values);

        final double mean = getMean(values);
        
        final double range = mm[1] - mm[0];

        final double[] ret = new double[values.length];
        for (int i = 0; i < values.length; ++i) {
            ret[i] = values[i] - mean;
        }
        return ret;
    }

    public static double[] getMinMaxValue(double[] values) {
        double min = Double.MAX_VALUE;
        double max = Double.MIN_VALUE;
        for (int i = 0; i < values.length; ++i) {
            if (values[i] < min) {
                min = values[i];
            }
            if (values[i] > max) {
                max = values[i];
            }
        }
        return new double[]{min, max};
    }

    public static double getMean(double[] values) {
        double ret = 0;
        for (int i = 0; i < values.length; ++i) {
            ret += values[i];
        }
        return ret /= values.length;
    }
    /*void convoluteWithOffset(const double* signal, const int signalLength, const int offset){
    mxAssert(signalLength>0, "Signal length is <=0");
    mxAssert(offset>=0, "Offset is <0");
    mxAssert(offset<signalLength, "Offset is >= signal length");
    
    iStart = offset;
    cStart = convolution + iStart;
    iEnd   = n-signalLength+offset+1;
    cEnd   = convolution + iEnd;
    const double *sIt, *vIt, *srcIt=value;
    const double *sEnd = signal + signalLength;
    double *cIt = cStart;
    double d;
    max = DBL_MIN;
    while(cIt<cEnd){
    d=0.0;
    vIt = srcIt++;
    sIt = signal;
    while(sIt<sEnd){
    //mexPrintf("i=%d v=%f  signal=%f  d=%f\n", i, *vIt, *sIt, d );
    d += (*vIt - mean) * (*sIt);
    ++vIt;
    ++sIt;
    }
     *cIt = d;
    if(d>max){
    max=d;
    }
    ++cIt;
    }
    }
    
    void convoluteWithOffsetZeroPad(const double* signal, const int signalLength, const int offset){
    
    
    convoluteWithOffset(signal, signalLength, offset);
    
    const double *vIt;
    double sigMean = 0.0;
    double d;
    for(int i=0; i<offset; ++i){
    const int sigStart = offset-i;
    //mexPrintf("i=%d, signal start=%d\n", i, sigStart);
    for(int j=sigStart; j<signalLength; ++j){
    sigMean += signal[j];
    }
    sigMean /= signalLength-sigStart;
    d=0.0;
    vIt = value;
    for(int j=sigStart; j<signalLength; ++j){
    d += (*vIt-mean)*(signal[j]-sigMean);
    ++vIt;
    }
    if(d>max){
    max=d;
    }
    convolution[i]=d;
    }
    iStart = 0;
    cStart = convolution;
    }
    
     */
}
