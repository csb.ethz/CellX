/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ImgGen;

import ImgGen.geom.ConvexHull;
import ImgGen.geom.Cross2D;
import cellxgui.view.ImagePanel;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;

/**
 *
 * @author cmayer
 */
public class TestConvexHull {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
         ImagePanel ip = new ImagePanel(generateImage(800, 600));
        ip.initMouse();
        ip.setShowHelp(false);
        JFrame frame = new JFrame();
        frame.add(ip);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setSize(600, 400);
        frame.setVisible(true);
         
    }

    private static BufferedImage generateImage(int w, int h) {
        final BufferedImage img = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = img.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        
        
        g2.translate(w/2.0, h/2.0);
        
        
        final double scale = 0.8*Math.min(w, h);
        int n=40;
        
        g2.setColor(Color.green);
        final Point2D[] points = new Point2D[n];
        for(int i=0; i<n; ++i){
            points[i] = new Point2D.Double( (Math.random()-0.5)*scale, (Math.random()-0.5)*scale);
        
            g2.drawOval((int)points[i].getX(), (int)points[i].getY(),2,2);
        }
        
        final Point2D[] convexHull = ConvexHull.getConvexHull(points);
        final Shape s = ConvexHull.createHullPath(convexHull);
        g2.setColor(Color.red);
        g2.draw(s);
        
        g2.setColor(Color.yellow);
        final Point2D p = ConvexHull.computeCentroid(convexHull);
        g2.draw(Cross2D.createCross(p, 10));
        
        
        
        g2.dispose();
        return img;
    }
    
    
    
    
    
    
}
