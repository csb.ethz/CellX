/*
 * TestLogo.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXUtilsLibrary
 *
 * CellXUtilsLibraryis free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * CellXUtilsLibrary is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package ImgGen;

import cellxgui.view.ImagePanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.JFrame;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class TestLogo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here



        ImagePanel ip = new ImagePanel(generateImage(1600, 1400));
        ip.initMouse();
        JFrame frame = new JFrame();
        frame.add(ip);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        frame.setLocation(400,400);
        frame.setSize(600, 400);
        frame.setVisible(true);


    }

    private static BufferedImage generateImage(int w, int h) {
        final BufferedImage img = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = img.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g2.setColor(new Color(210, 10, 5));
        g2.fillRect(0, 0, w, h);


        g2.translate(w / 2, h / 2);
        
        final double w2 = Math.min(w, h)*0.05;

        SpreadRayRenderer.render(g2, new Dimension(w, h), new Color(150, 150, 150, 0), new Color(220, 200, 0, 230), 0, 200, 10, w2);

        g2.dispose();
        return img;
    }

 
}
