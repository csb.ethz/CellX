/*
 * TestBezierInterpolation.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXUtilsLibrary
 *
 * CellXUtilsLibraryis free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * CellXUtilsLibrary is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package ImgGen;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.RenderingHints;

import javax.swing.JFrame;
import org.apache.batik.dom.svg.SVGDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.apache.batik.swing.JSVGCanvas;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.svg.SVGDocument;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class TestBezierInterpolation {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        DOMImplementation impl = SVGDOMImplementation.getDOMImplementation();
        String svgNS = SVGDOMImplementation.SVG_NAMESPACE_URI;
        SVGDocument doc = (SVGDocument) impl.createDocument(svgNS, "svg", null);

        SVGGraphics2D g2 = new SVGGraphics2D(doc);

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_NORMALIZE);
 
        g2.setSVGCanvasSize(new Dimension(400,300));

        final double x[] = new double[]{40, 120, 150, 180};
        final double y[] = new double[]{100, 30, 160, 230};
        g2.setColor(Color.black);
        for(int i=0; i<x.length; ++i){
            g2.drawLine((int)x[i], (int)y[i]-3, (int)x[i], (int)y[i]+3);
            g2.drawLine((int)x[i]-3, (int)y[i], (int)x[i]+3, (int)y[i]);
        }
    
        g2.setColor(Color.red);
        
        g2.draw(BezierSpline.createBezierSpline(x, y, BezierSpline.EndMode.SMOOTH_START_AND_END, 0.3));
        
        
        
        final double[][] circ =  new double[][]{new double[]{100,100},new double[]{100,150},new double[]{150,150},new double[]{150,100} };
        g2.setColor(Color.black);
        for(int i=0; i<circ.length; ++i){
            g2.drawLine((int)circ[i][0], (int)circ[i][1]-3, (int)circ[i][0], (int)circ[i][1]+3);
            g2.drawLine((int)circ[i][0]-3, (int)circ[i][1], (int)circ[i][0]+3, (int)circ[i][1]);
        }
        
        g2.setColor(Color.red);
        g2.draw(BezierSpline.createBezierSpline(circ, BezierSpline.EndMode.SMOOTH_START_AND_END, 0.5));            

        
        
        JSVGCanvas canvas = new JSVGCanvas();

        g2.getRoot(doc.getRootElement());
        canvas.setSVGDocument(doc);
        canvas.setEnableImageZoomInteractor(true);
        canvas.setEnableZoomInteractor(true);

        JFrame frame = new JFrame();
        frame.getContentPane().add(canvas);
        frame.pack();
        frame.setSize(600, 600);

        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        
    }
}
