/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ImgGen.geom;

import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.util.Arrays;
import java.util.Comparator;

/**
 *
 * @author cmayer
 *
 * Adapted from
 * http://www.algorithmist.com/index.php/Monotone_Chain_Convex_Hull.java
 */
public class ConvexHull {

    public static Point2D[] getConvexHull(Point2D[] points) {
        
        
        if( points.length==1 || points.length==2 ) return points;
        
        Arrays.sort(points, new Comparator<Point2D>() {

            @Override
            public int compare(Point2D t, Point2D t1) {
                if (t.getX() == t1.getX()) {
                    return (int) Math.signum(t.getY() - t1.getY());
                } else {
                    return (int) Math.signum(t.getX() - t1.getX());
                }
            }
        });
        final int n = points.length;
        final Point2D[] ans = new Point2D[2 * n];	 // In between we may have a 2n points
        int k = 0;
        int start = 0;				 // start is the first insertion point
        for (int i = 0; i < n; i++) {
            final Point2D p = points[i];
            while (k - start >= 2 && eval(p, ans[k - 1], ans[k - 2]) > 0) {
                k--;
            }
            ans[k++] = p;
        }
        k--; 						// drop off last point from lower layer
        start = k;
        for (int i = n - 1; i >= 0; i--) {
            final Point2D p = points[i];
            while (k - start >= 2 && eval(p, ans[k - 1], ans[k - 2]) > 0) {
                k--;
            }
            ans[k++] = p;
        }
        k--;						// drop off last point from top layer
        return Arrays.copyOf(ans, k);
    }

    public static double eval(Point2D p, Point2D k1, Point2D k2) {
        final double x1 = p.getX() - k1.getX();
        final double y1 = p.getY() - k1.getY();
        final double x2 = p.getX() - k2.getX();
        final double y2 = p.getY() - k2.getY();
        return x1 * y2 - x2 * y1;
    }

    public static GeneralPath createHullPath(Point2D[] points) {
        final GeneralPath gp = new GeneralPath();
        Point2D p = points[0];
        final int n = points.length;
        gp.moveTo(p.getX(), p.getY());
        for (int i = 1; i <= n; ++i) {
            p = points[i % n];
            gp.lineTo(p.getX(), p.getY());
        }
        return gp;
    }

    public static Point2D computeCentroid(Point2D[] convexHullPoints) {

        double x = 0, y = 0;
        double signedArea = 0.0;
        double x0, y0, x1, y1;
        double a;

        final int n = convexHullPoints.length;

        if( n==1 ) return convexHullPoints[0];
        
        if( n==2 ) return getMidPoint(convexHullPoints[0], convexHullPoints[1]);
        
        for (int i = 0; i <= n; ++i) {
            x0 = convexHullPoints[i % n].getX();
            y0 = convexHullPoints[i % n].getY();
            x1 = convexHullPoints[(i + 1) % n].getX();
            y1 = convexHullPoints[(i + 1) % n].getY();
            a = x0 * y1 - x1 * y0;
            signedArea += a;
            x += (x0 + x1) * a;
            y += (y0 + y1) * a;
        }
        signedArea *= 0.5;
        x /= (6 * signedArea);
        y /= (6 * signedArea);
        return new Point2D.Double(x, y);
    }
    
        
    public static Point2D getMidPoint(Point2D p1, Point2D p2){
        final double dx = p2.getX()-p1.getX();
        final double dy = p2.getY()-p1.getY();   
        return new Point2D.Double(p1.getX()+0.5*dx, p1.getY()+0.5*dy);       
    }

}
