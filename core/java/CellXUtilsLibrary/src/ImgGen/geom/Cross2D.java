/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ImgGen.geom;

import java.awt.Shape;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;

/**
 *
 * @author cmayer
 */
public class Cross2D {

    public static Shape createCross(Point2D p, double len) {

        final GeneralPath gp = new GeneralPath();
        gp.moveTo(p.getX() - len, p.getY());
        gp.lineTo(p.getX() + len, p.getY());
        gp.moveTo(p.getX(), p.getY() - len);
        gp.lineTo(p.getX(), p.getY() + len);
        return gp;

    }

    public static Shape createCross(Point2D p) {
        return createCross(p, 1.0);
    }
}
