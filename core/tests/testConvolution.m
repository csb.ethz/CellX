%
%
%
%
clear; clc; close;


thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);

addpath('../mclasses');
addpath('../mfunctions');
addpath('../mex');
addpath('../mex/maxflow');

inImage = double(imread('../data/img1.tif')); 

%rawImage = imcrop(inImage, [260,200,150,150]);

rawImage = imcrop(inImage, [245,780,150,150]);
minimumBitValue = min(rawImage(:));
maximumBitValue = max(rawImage(:));
variableBitRange = maximumBitValue - minimumBitValue

% normalize the image to 0-1 range
borderWidth =25;
normImg = wiener2((rawImage-minimumBitValue) / variableBitRange , [3 3]);

height =151;
width=151;
img = zeros(height+2*borderWidth,width+2*borderWidth);
img(borderWidth+1:height+borderWidth, borderWidth+1:width+borderWidth) = normImg;

config = CellXConfiguration();
config.setMembraneIntensityProfile([0.6,0.4,0.2,0.1,0.1,0.2,0.4,0.7,0.8,0.9,0.8,0.6,0.5,0.4]);

config.getRayConvolutionValueThreshold()

%imagesc(img);

[convImg maxVals] = computeRayConvolution(70,70, img, config.membraneIntensityProfile, 6);
hold on;
imagesc(convImg);
axis image;
plot(70,70, 'r-o');
%figure;
%hist(maxVals, 100);




