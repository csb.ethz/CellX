clear; clc; close;


thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);

addpath('../mclasses');
addpath('../mfunctions');
addpath('../mex');
addpath('../mex/maxflow');


config = CellXConfiguration();

config.setMembraneIntensityProfile([0.6,0.4,0.2,0.1,0.1,0.2,0.4,0.7,0.8,0.9,0.8,0.6,0.5,0.4]);
config.setMembraneLocation(6);
config.setMembraneWidth(3);
config.setSeedRadiusLimit([10 27]);
config.setMaximumCellLength(120);
config.setHoughTransformOnCLAHEImage(1);
config.setCropRegion(200,200,300,300);
config.setDebugLevel(1);
config.check();

filename = 'configExportTest.xml';

config.membraneIntensityProfile

config.toXML(filename)

otherconfig = CellXConfiguration()

otherconfig.fromXML(filename);
otherconfig


constr = CellXConfiguration.fromXML(filename)
