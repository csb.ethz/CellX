
clear all;

thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);

addpath('../mclasses');
addpath('../mfunctions');
addpath('../mex');

h=801;
w=1201;
img = zeros(h,w);

tic;
rays = createRayImage(w/2, h/2, img, 20);
toc;
imtool(rays)


signal = [1 1 2 3];

signal = signal - sum(signal)/length(signal);

target = [1 1 1 1 2 3 4 3 2 1 1 1 1 2 1];

r = convoluteOneRay(target, signal, 2);

plot(r);
hold on;
plot(target, 'g');



