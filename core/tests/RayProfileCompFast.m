function [Maxccprofile,qzupd, qzfastCount] = RayProfileCompFast(curmembrpixels1,CurrImageRegion,curcentroid1,MembraneRefSignal,midsignpoint)

imwidth = size(CurrImageRegion,2);
imheight = size(CurrImageRegion,1);
qzfast = zeros(imheight,imwidth);  
qzfastCount = zeros(imheight,imwidth); 

Maxccprofile=zeros(size(curmembrpixels1,1),1);


cmp = 0;
for nmempix= 1:size(curmembrpixels1,1)
    qzfastPart = zeros(imheight,imwidth);
     qzfastCountPart = zeros(imheight,imwidth);
    %first construct the line
    memcood = curmembrpixels1(nmempix,:);
    if numel(curcentroid1)==2
        % construct the line equation
        [cx,cy,imrayprofile] =...
            improfile(CurrImageRegion,[curcentroid1(1)  memcood(1)],[curcentroid1(2)   memcood(2)]);
    else
        error('CC - Ray profiles cant be computed\n')
    end
    if length(imrayprofile)>length(MembraneRefSignal)
        if (isempty(find(isnan(imrayprofile),1)) && isempty(find(isinf(imrayprofile),1)))
            cmp = cmp+1;
            
            [rcv,lags] = xcov(imrayprofile,MembraneRefSignal);
            Nst = find(lags==midsignpoint);
            ccprofile = rcv(Nst:end+midsignpoint);
            Maxccprofile(cmp,1)=max(ccprofile);
            
            % find linear indices
            linind = sub2ind([imheight,imwidth],round(cy),round(cx));
            
            % pass them to the final matrix
            qzfastPart(linind) = ccprofile;
            qzfast = qzfast+qzfastPart;
            % and count
            qzfastCountPart(linind) = 1;
            qzfastCount = qzfastCount + qzfastCountPart;
            
        else
            % remove a line from  the storing matrices
            Maxccprofile = Maxccprofile(1:end-1);
            fprintf('Problem in Ray %d : NaNs or Infs are produced in improfile \n',nmempix)
        end
    else
        fprintf('Ray %d was not evaluated: has length < Membrane Pattern \n',nmempix)
    end
end % end pixel number

qzupd = qzfast./qzfastCount;

