clear;clc;close all

dbstop if warning

addpath('../mclasses')

ameansCases = [20 50 80;...
               50 65 80;...
               60 70 80;...
               70 75 80;...
               40 40 80;...
               80 80 80];
ameansCases = ameansCases/100;
aStdCases = [0.02 0.02 0.02];

NrGauss = [1:4];
RefSampleSize = 1e2;
NrCases = size(ameansCases,1);
AICvals = zeros(NrCases ,length(NrGauss));
BICvals = zeros(NrCases ,length(NrGauss));
NlogLvals = zeros(NrCases ,length(NrGauss));
MeanVals = cell(NrCases ,1);
ModelsMeanVals= cell(NrCases ,1);
InitDistrMeans = zeros(NrCases ,3);
SelectedNumberOfModels = zeros(NrCases ,1);
stepcnt=0;
NoiseRemoveTag = 0;
for kcase = 1:NrCases
    
    stepcnt=stepcnt+1;
    % produce the test vector values initial
    a1mean =ameansCases(kcase,1);
    a1 = a1mean + aStdCases(1).*randn(5*RefSampleSize ,1);
    
    a2mean = ameansCases(kcase,2);
    a2 = a2mean +  aStdCases(2)*randn(20*RefSampleSize ,1);
    
    a3mean = ameansCases(kcase,3);
    a3 = a3mean + aStdCases(3).*randn(50*RefSampleSize ,1);
    
    % noise term
    a4 = min(ameansCases(:)) + (max(ameansCases(:))+min(ameansCases(:)))*rand(RefSampleSize ,1);
    
    a = [a1;a2;a3;a4];
    
    SampleSize = length(a);
    
    
    nrbins=100;
    [n,xout] = hist(a,nrbins);
    if 0
        % plot the initial distribution
        figure;
        bar(xout,n/SampleSize)
    end
    
    
    if NoiseRemoveTag ==1
        % remove tails
        MinNumberOfCountsPerBin = 0.1*length(a)/nrbins;
        % find the accepted bins
        AccBins = find(n>MinNumberOfCountsPerBin);
        if 0
            figure;
            bar(xout(AccBins),n(AccBins)/SampleSize)
        end
        % restore a
        a_new=[];
        for nrab = 1:length(AccBins)
            a_new = [a_new;repmat(xout(AccBins(nrab)),n(AccBins(nrab)),1)];
        end
    else
        a_new = a;
    end
    
    %% run the GMM implementation
    
    tic
    x = CellXMixtureModel(NrGauss);
    x.computeMixtureModel(a_new);
    toc
    
    MeanVals{stepcnt,1} =  x.gaussianMeanVals;
    AICvals( stepcnt,:) =  x.gaussianAICVals';
    BICvals( stepcnt,:) =  x.gaussianBICVals';
    NlogLvals( stepcnt,:) =  x.gaussianNlogLVals';
    InitDistrMeans(stepcnt,:) =  [a3mean a2mean a1mean];
    SelectedNumberOfModels(stepcnt,1) =length(x.gaussianMeanVals);
    ModelsMeanVals{stepcnt,1} = x.gaussianmuVals;  
end


% substract the min NlogL for each case
OptModelIndex = zeros(NrCases,1);
OptModelNlogL= zeros(NrCases,1);
for k= 1:NrCases
    curNlogLVals = NlogLvals(k,:);
    curNlogLdiffs = curNlogLVals - min(curNlogLVals);
    NlogLThresh = abs(0.025*min(curNlogLVals)); 
    % find the ones which are less than the threshold
    % and take the first one
    ModelsThresholded = find(curNlogLdiffs <NlogLThresh);
    OptModelIndex(k,1) =  ModelsThresholded(1);
    OptModelNlogL(k,1) = curNlogLVals(OptModelIndex(k,1));
end

%ploting
figure;
plot( NlogLvals')
legend('case1','case2','case3','case4','case5','case6');
hold on
plot(OptModelIndex,OptModelNlogL,'ro')





