% define a normalized membrane signal
clear all;

thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);

addpath('../mclasses');
addpath('../mfunctions');
addpath('../mex');
addpath('../mex/maxflow');

tmpRS = [ -0.2202   -0.3452   -0.4702   -0.4702   -0.3952   -0.1327...
        0.1048    0.3048  0.5298    0.5298    0.3798    0.1548    0.0298];

    
% define midpoint
tempMidpoint = 5;    

% define a single Ray intensity profile    
tmpRay = [zeros(1,25) tmpRS zeros(1,25-length(tmpRS ))];
%tmpRay = tmpRay- mean(tmpRay);

% define initial image
%tmpImg = 0.5*ones(101,101);
tmpImg = zeros(101,101);
% pass a single intensity ray into the image
tmpImg(51,51:100) = tmpRay;


%% OLD correlation implementation
nx1 = 101;
ny1 = 101;
leftImPart = [1*ones(nx1,1),[1:nx1]'];
rightImPart = [ny1*ones(nx1,1),[1:nx1]'];
upImapart = [[1:ny1]',ones(ny1,1)];
lowImapart = [[1:ny1]',nx1*ones(ny1,1)];
curmembrpixels1 = [leftImPart; rightImPart;upImapart;lowImapart];

%  RAY THROWING  &  RAY CROSS-COVARIANCE COMPUTATION
% for each Membrane pixel, throw a ray wrt to the center

[Maxccprofile, qz, qzc] =...
    RayProfileCompFast(curmembrpixels1,tmpImg ,[51 51],tmpRS ,-tempMidpoint);

% take the back the ConvolutionRay
ccprofile = qz(51,51:100);


%% Christian's implementation with same midpoint

% run the convolution
[tmpImgRes1 maxVals] = computeRayConvolution( ...
    51, ...
    51, ...
    tmpImg, ...
    tmpRS, ...
    tempMidpoint);

% take out the corresponding ray
ccprofile1 = tmpImgRes1(51,51:100);
% print some information


%% Christian's implementation with midpoint +1

% run the convolution
[tmpImgRes2 maxVals crossings] = computeRayConvolution( ...
    51, ...
    51, ...
    tmpImg, ...
    tmpRS, ...
    tempMidpoint+1);

% take out the corresponding ray
ccprofile2 = tmpImgRes2(51,51:100);
% print some information


%% compare results

figure;
plot([ ccprofile' ccprofile1' ccprofile2'])
legend('Sotiris','Christian1','Christian2' );


qzc(1:7,:)=0;
qzc(:,1:7)=0;
qzc(95:101,:)=0;
qzc(:,95:101)=0;

qzc2=qzc;
qzc2(47:55,47:55)=0;


crossings = double(crossings);
crossings2 = crossings;
crossings2(47:55,47:55)=0;

subplot(2,2,1)
imagesc(qzc2)
subplot(2,2,2)
imagesc(crossings2);

subplot(2,2,3)


imagesc(qzc-crossings);

