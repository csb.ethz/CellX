clear; clc; close;


thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);

addpath('../mclasses');
addpath('../mfunctions');
addpath('../mex');
addpath('../mex/maxflow');


config = CellXConfiguration.readXML('../data/img2config.xml');

config.setDebugLevel(1);
%config.setCropRegion(1,1,300,300);
fileSet = CellXFileSet(1, '../data/img2.tif');

fileSet.setResultsDirectory('/tmp/img2');

seg = CellXSegmenter(config, fileSet);

seg.run();

CellXResultWriter.writeTxtSegmentationResults(...
    fileSet, ...
    seg.getDetectedCells(), ... 
    config);


CellXResultWriter.writeMatSegmentationResults(...
    fileSet.getSeedsMatFileName(), ...
    seg.getDetectedCells() );

