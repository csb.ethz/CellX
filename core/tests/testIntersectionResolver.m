


clear; clc; close;


thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);

addpath('../mclasses');
addpath('../mfunctions');
addpath('../mex');
addpath('../mex/maxflow');

config = CellXConfiguration();

config.setMembraneIntensityProfile([0.6,0.4,0.2,0.1,0.1,0.2,0.4,0.7,0.8,0.9,0.8,0.6,0.5,0.4]);
config.setMembraneLocation(6);
config.setMembraneWidth(3);
config.setSeedRadiusLimit([10 27]);
config.setMaximumCellLength(120);
config.setHoughTransformOnCLAHEImage(0);
config.setSeedSensitivity(0.1);
%config.setDebugLevel(5);
config.check();


load('../data/seeds.mat');
img = imread('../data/img1.tif');
dim = size(img)



tic;
ir = CellXSeedIntersectionResolver(config,seeds,dim);

ir.run();
toc;


img = double(img);

maxV = max(img(:));
minV = min(img(:));
varV = maxV-minV;
img = (img-minV)/varV;


r = img;
g = img;
b = img;

n = numel(ir.mergedSeeds);
for i=1:n
    r(ir.mergedSeeds(i).pixelList) = 1;

    r(round(ir.mergedSeeds(i).centroid(2)), round(ir.mergedSeeds(i).centroid(1))) = 0;
    
    bb = round(ir.mergedSeeds(i).boundingBox);
    r(bb(2), bb(1):(bb(1)+bb(3)-1)) = 1;
    r((bb(2)+bb(4)-1), bb(1):(bb(1)+bb(3)-1)) = 1;
    r(bb(2): (bb(2)+bb(4)-1), bb(1)) = 1;
    r(bb(2): (bb(2)+bb(4)-1), (bb(1)+bb(3)-1)) = 1;
    
    sources = ir.mergedSeeds(i).sourceSeeds;
    
    for k =1:numel(sources)
       sIdx = sources(k);
       g(ir.seeds(sIdx).perimeterPixelListLindx) = 0.4+0.6*rand();
       b(ir.seeds(sIdx).perimeterPixelListLindx) = 0.4+0.6*rand();
    end
end


for i =1:numel(ir.clusters)
    ov = ir.clusters{i};
    for j = 1:numel(ov)
        s = ir.seeds(ov(j));
        r(s.perimeterPixelListLindx) = 0.6 + 0.4*rand();
        %g(s.perimeterPixelListLindx) = 0.8+0.2*rand();
        %b(s.perimeterPixelListLindx) = 0.4+0.6*rand();
    end
end


for i = 1:numel(ir.resolvedSeeds)
    s = ir.resolvedSeeds(i);
    
    
      bb = round(s.cropRegion);
    r(bb(2), bb(1):(bb(1)+bb(3)-1)) = 1;
    r((bb(2)+bb(4)-1), bb(1):(bb(1)+bb(3)-1)) = 1;
    r(bb(2): (bb(2)+bb(4)-1), bb(1)) = 1;
    r(bb(2): (bb(2)+bb(4)-1), (bb(1)+bb(3)-1)) = 1;
    
    r(s.perimeterPixelListLindx) =1;
    g(s.pixelList) = 0.2+0.8*rand();
    b(s.pixelList) = 0.4+0.6*rand();
end

rgb = cat(3, r, g, b);
figure;
imagesc(rgb);
 
 