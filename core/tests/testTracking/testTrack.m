clear; clc; close;
dbstop if error

thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);

addpath('../../mclasses');
addpath('../../mfunctions');
addpath('../../mex');
addpath('../../mex/maxflow');

% only for development
%makeMex;

runSegmentation=0;

if runSegmentation
    
    config = CellXConfiguration();
    
    config.setMembraneIntensityProfile([ 0 0 1 1]);
    config.setMembraneLocation(2);
    config.setMembraneWidth(2);
    config.setSeedRadiusLimit([10 50]);
    config.setMaximumCellLength(100);
    config.setHoughTransformOnCLAHEImage(0);
    config.setGraphCutOnCLAHEImage(0)
    config.setSeedSensitivity(0.2);
    config.setFluoAlignPixelMove(0);
    %config.setCropRegion(610, 900, 270, 250);
    config.setDebugLevel(0);
    config.check();
    
    save('./config.mat','config');
    
    NrFrames = 10;
    
    AllFilesNames = cell(NrFrames,3);
    
    fileHandler = CellXFileHandler('./fileSetsTestTrack.txt');

    
    for k=1:NrFrames
        
        filename  = ['./img' num2str(k) '.tiff'];
        AllFilesNames{k,1} = filename;

        fSet = fileHandler.fileSets(k);
        
        cellXSegmenter = CellXSegmenter(config, fSet);
        
        cellXSegmenter.run();
        
        segmentedCells = cellXSegmenter.getDetectedCells();
        
        cellXIntensityExtractor = CellXIntensityExtractor(config, fSet, segmentedCells);
        
        cellXIntensityExtractor.run();
        
        % produce the segmentation mask
        %--3. get the fluo image background value
        SegMask =zeros(size(cellXSegmenter.image));
        for nrc = 1:length(segmentedCells)
            curPixelList = segmentedCells(nrc).cellPixelListLindx;
            SegMask(curPixelList) = nrc;
            %segmentedCells(nrc).segmentationIndex = nrc;
            %segmentedCells(nrc).frameIndex  = k;
        end
        % end temp code
        % save the segmentation mask
        maskfilename =  ['./mask_' num2str(k) '.mat'];
        save(maskfilename,'SegMask');
        AllFilesNames{k,3} = maskfilename;
        
        pf2A01 = figure;%('DockControls','off','Visible','off');
        imagesc(SegMask);
        hold on
        for nrc = 1:length(segmentedCells)
             curcen = segmentedCells(nrc).centroid;
             text(curcen(1),curcen(2),num2str(nrc),...
                  'FontSize',12,'BackgroundColor',[1 1 1])
        end
        fnc = ['figOfmask_' num2str(k) '.png'];
        print(pf2A01,'-dpng','-r300',fnc);
        
        % save the cells object
        cellfilename =  ['./seed_' num2str(k) '.mat'];
        AllFilesNames{k,2} = cellfilename;
        save(cellfilename,'segmentedCells');
        
    end
    save('matlab.mat')
else
    
    load('matlab.mat')
    
end
%% Run the CellXTracker

tracker = CellXTracker(config,fileHandler);
tracker.run();

CellXImgGen.generateLineageTree(tracker.trackingInfoMatrix);
