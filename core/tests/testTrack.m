clear; clc; close;
dbstop if error

thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);

addpath('../mclasses');
addpath('../mfunctions');
addpath('../mex');
addpath('../mex/maxflow');


config = CellXConfiguration();

config.setMembraneIntensityProfile([ 0 0 1 1]);
config.setMembraneLocation(2);
config.setMembraneWidth(2);
config.setSeedRadiusLimit([10 50]);
config.setMaximumCellLength(100);
config.setHoughTransformOnCLAHEImage(0);
config.setGraphCutOnCLAHEImage(0)
config.setSeedSensitivity(0.2);
config.setFluoAlignPixelMove(0);
%config.setCropRegion(610, 900, 270, 250);
config.setDebugLevel(0);
config.check();


%save('./config.mat','config');

%fileHandler = CellXFileHandler.fromXML('../data/testTracking/fileSeries.xml', '../data/testTracking/results');
fileHandler = CellXFileHandler.fromXML('../data/testTracking/fileSeriesLocal.xml', '../data/testTracking/results');

for k=1:fileHandler.getNumberOfFileSets
    
    fSet = fileHandler.fileSets(k);
    
    % 1. run segmentation
    cellXSegmenter = CellXSegmenter(config, fSet);
    
    cellXSegmenter.run();
    
    segmentedCells = cellXSegmenter.getDetectedCells();
      
    % 2. run intensity extraction
    cellXIntensityExtractor = CellXIntensityExtractor(config, fSet, segmentedCells);
    
    cellXIntensityExtractor.run();
    
    % 3. run Result writer
    CellXResultWriter.writeMatSegmentationResults(fSet.getSeedsMatFileName(), segmentedCells);
    
    CellXResultWriter.writeSegmentationMask(...
        fSet.getMaskMatFileName(), ...
        segmentedCells, ...
        cellXSegmenter.getInputImageDimension());
      
    % write TXT result of the current segmentation
    CellXResultWriter.writeTxtSegmentationResults(fSet,segmentedCells,config);
end

%% Run the CellXTracker

tracker = CellXTracker(config, fileHandler);
tracker.run();

% save the track data
CellXResultWriter.writeMatTrackerData(fileHandler.trackingMatResultFile,tracker.getTrackData())

% write the track results
CellXResultWriter.writeTxtTrackResults(fileHandler,tracker.getTrackData(),config);

% create lineage plot
CellXImgGen.generateLineageTree(tracker.trackingInfoMatrix);


CellXImgGen.generateLineageTree(tracker.trackingInfoMatrix);
