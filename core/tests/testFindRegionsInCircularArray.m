%
%
%
clear; clc; close;

thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);
addpath('../mex');


x = false(10,1);
centers = findRegionCentersInCircularArray(x, 0, 0);
if( sum(centers)~=0 )
    error('Test failed');
end

centers = findRegionCentersInCircularArray(x, 0, 9);
if( sum(centers)~=0 )
    error('Test failed');
end

centers = findRegionCentersInCircularArray(x, 0, 10);
if( sum(centers)~=1 )
    error('Test failed');
end

centers = findRegionCentersInCircularArray(x, 10, 10);
if( sum(centers)~=1 )
    error('Test failed');
end

centers = findRegionCentersInCircularArray(x, 11, 10);
if( sum(centers)~=0 )
    error('Test failed');
end


x(4:6) = 1;
centers = findRegionCentersInCircularArray(x, 0, 0);

if( sum(centers)~=1 || centers(5)~=1)
    error('Test failed');
end


x(7) = 1;
centers = findRegionCentersInCircularArray(x, 0, 0);
if( sum(centers)~=1 || centers(5)~=1)
    error('Test failed');
end


y = false(10,1);
y(2)   = true;
y(7:9) = true;

centers = findRegionCentersInCircularArray(y, 0, 0);
if( sum(centers)~=2 || centers(8)~=1 || centers(2)~=1)
    error('Test failed');
end

centers = findRegionCentersInCircularArray(y, 2, 0);
if( sum(centers)~=1 || centers(8)~=1 )
    error('Test failed');
end

centers = findRegionCentersInCircularArray(y, 2, 2);
if( sum(centers)~=1 || centers(9)~=1 )
    error('Test failed');
end

centers = findRegionCentersInCircularArray(y, 2, 3);
if( sum(centers)~=1 || centers(9)~=1 )
    error('Test failed');
end

centers = findRegionCentersInCircularArray(y, 2, 4);
if( sum(centers)~=1 || centers(5)~=1 )
    error('Test failed');
end


z = false(10,1);
z([2,4,6]) = true;
[centers smoothed] = findRegionCentersInCircularArray(z, 0, 1);
if( sum(centers)~=1 || centers(4)~=1 )
    error('Test failed');
end



fprintf('Tests ok.\n');


