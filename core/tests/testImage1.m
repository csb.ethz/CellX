clear; clc; close;


thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);

addpath('../mclasses');
addpath('../mfunctions');
addpath('../mex');
addpath('../mex/maxflow');


config = CellXConfiguration.readXML('../data/img1config.xml');

config.setCropRegion(100,100,500,500);
fileSet = CellXFileSet(1, '../data/img1.tif');
seg = CellXSegmenter(config, fileSet);

seg.run();


fileSet.setResultsDirectory('/tmp');


fprintf('Writing results files ...\n');



img = CellXImageIO.loadToGrayScaleImage(fileSet.oofImage, config.cropRegionBoundary);
img = CellXImageIO.normalize( img );
CellXImgGen.createControlImageWithCellIndices( seg.getDetectedCells(), img,  fileSet.controlImageFile);


himg = CellXImgGen.generateHoughTransformImage(seg.seeds, img);
imtool(himg);

img = CellXImgGen.generateIntensityExtractionControlImage( seg.getDetectedCells(), img);

imtool(img);


CellXResultWriter.writeTxtSegmentationResults(...
    fileSet, ...
    seg.getDetectedCells(), ...
    config);


CellXResultWriter.writeMatSegmentationResults(...
    fileSet.getSeedsMatFileName(), ...
    seg.getDetectedCells() );

imtool('/tmp/control_1.png')
