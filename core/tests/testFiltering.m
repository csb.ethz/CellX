clear; clc;


thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);

addpath('../mclasses');
addpath('../mfunctions');
addpath('../mex');
addpath('../mex/maxflow');

% only for development
%makeMex;



config = CellXConfiguration();

config.setMembraneIntensityProfile([0.6,0.4,0.2,0.1,0.1,0.2,0.4,0.7,0.8,0.9,0.8,0.6,0.5,0.4]);
config.setMembraneLocation(6);
config.setMembraneWidth(3);
config.setSeedRadiusLimit([10 27]);
config.setMaximumCellLength(120);
config.setHoughTransformOnCLAHEImage(0);
config.setSeedSensitivity(0.1);

% ok - seed between cells is filtered out
%config.setCropRegion(100,100,500,500);
% not ok, seed between cells is ok and different cut
config.setCropRegion(100,100,600,600);

config.setDebugLevel(1);
config.check();


fs = CellXFileSet(1,'../data/img1.tif')

cellXSegmenter = CellXSegmenter(config, fs);

%dbstop if error;

cellXSegmenter.run();