%%  case of Shapes
%  crowded abberant yeast cells 
if ccase==321
    settings = getSettings();
    settings.UseInit = 1;
    settings.UseAdaptForGC = 0 ;
    settings.NonConstantPattern = 1 ;
    settings.FigureNumber = 2;
    settings.debug = 1;
    settings.radMat = [6 15];
    settings.ElongationLength = 90;
    %settings.CropInteract = 1;
    settings.CropRegion = [105 801 230 210];
    settings.IsNotConvex = 1;
    settings.ExpansionThresh = 0.5; 
    settings.MembraneSignal = [ 0.4870,    0.2840,    0.1590,    0.1360,    0.1560,    0.2496,    0.5674, ...
                                0.6835,    0.7290,    0.7212,    0.5576,    0.5006,    0.2738 ];
    settings.MembraneSignalLocation = 7;    
    settings.PercOfSuccRayThreshold = 0.85;
    settings.MembraneWidth = 3;
    settings.SeedSensitivity  = 0.35; 
    
    settings.bf_out_of_focus_image = [loc 'Figure3_crowded/Shapes/AbberantYeastCells/pos1/BF_position040521_time0001.tif'];
    settings.bf_in_focus_image     = [loc 'Figure3_crowded/Shapes/AbberantYeastCells/pos1/BF_position040516_time0001.tif'];
    settings.seg_image_out = '../ResultFolder/seg_test_shapes.png';
    settings.results_file  = '../ResultFolder/results_test_shapes.txt';
    settings.frameno =  1;

   checkSettings(settings);
   main(settings);
end
