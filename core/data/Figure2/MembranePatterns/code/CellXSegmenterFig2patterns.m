classdef CellXSegmenterFig2patterns < CellXSegmenter
    
    methods
        % call the constructor of the superclass to initialize an instance
        function obj = CellXSegmenterFig2patterns(config, fileSet)
            obj@CellXSegmenter(config, fileSet);
        end
    end
    
    % only protected and public methods can be overwritten by subclasses
    methods (Access=public)
        
        
        function run(this)      
            tic;
            fprintf(' Segmenting cells ...\n');
            
            this.loadImages();
            
            try
                this.computeHoughTransform();
            catch exc
                rethrow(exc);
            end
            
            if( this.config.calibrationMode==1 )
                return;
            end 
            
            this.detectMembranes();           
            validator = CellXSeedValidator(this.config, this.seeds, size(this.image));       
            validator.run();
            
            
            ir = CellXSeedIntersectionResolver(this.config, this.seeds, size(this.image));
            ir.run();            
            this.seeds = ir.seeds;                     
            if( this.config.debugLevel > 0 )
                img = CellXImgGen.generateSegmenterDebugImage(this.seeds, this.image);
                imtool(img);
            end 
            
            
%              %------plot mergeImage
%             % cell ares superimposed on initial image
%             offScreenFig = figure('visible','off');
%             colorVec = [1 0 0;0 1 0];
%             fimage = zeros(size(this.image,1),size(this.image,2),3);
%             perc1 = 0.6;
%             fimage(:,:,1) = perc1*this.image;
%             fimage(:,:,2) = perc1*this.image;
%             fimage(:,:,3) = perc1*this.image;
%             for nrc =1:numel(this.seeds)
%                 cellList = this.seeds(nrc).cellPixelListLindx;
%                 tempImage = zeros(size(this.image,1),size(this.image,2));
%                 tempImage(cellList) = 1;
%                 if nrc==1
%                     fimage(:,:,1) = (1-perc1)*tempImage + fimage(:,:,1);
%                 else
%                     fimage(:,:,2) = (1-perc1)*tempImage + fimage(:,:,3);
%                 end
%                 %[perListRows,perListCols] = ind2sub(size(this.image),perList);
%                 hold on 
%                 %plot(perListCols,perListRows,'.','color',colorVec(nrc,:))
%                 Xc=this.seeds(nrc).houghCenterX;
%                 Yc=this.seeds(nrc).houghCenterY;
%                 text(Xc,Yc,['\fontsize{16}{' ['S' num2str(nrc)] '}' ],'Color',colorVec(nrc,:),...
%                     'HorizontalAlignment','center','BackgroundColor',[1 1 1],'EdgeColor','black');
%             end
%             image(fimage)
%             for nrc =1:numel(this.seeds)
%                 hold on 
%                 Xc=this.seeds(nrc).houghCenterX;
%                 Yc=this.seeds(nrc).houghCenterY;
%                 text(Xc,Yc,['\fontsize{16}{' ['S' num2str(nrc)] '}' ],'Color',colorVec(nrc,:),...
%                     'HorizontalAlignment','center','BackgroundColor',[1 1 1],'EdgeColor','black');
%             end
%             axis off       
%             locF2=this.fileSet.resultsDir;
%             fnc = [locF2 'mergeImage'];
%             print(offScreenFig,'-depsc','-r300',fnc);
%             %------end plot
            
            %------ plot cell Image
            offScreenFig7= figure('visible','off');
            colormap(gray)
            imagesc(this.image);
            axis off
            box off
            for sc=1:numel(this.seeds)
                fseed = this.seeds(sc);
                if fseed.skipped==0
                    cellPixels = fseed.cellPixelListLindx;
                    [perListRows,perListCols] = ind2sub(size(this.image),cellPixels);
                    hold on
                    plot(perListCols,perListRows,'.','color',0.2 +0.7*rand(1,3),'MarkerSize',10)
                    axis off
                end
            end
            locF2=this.fileSet.resultsDir;
            fnc = [locF2 'finalCellImage'];
            print(offScreenFig7,'-depsc','-r300',fnc);
            %------ end plot membrane Image
            
            %------ plot membrane Image
            offScreenFig6 = figure('visible','off');
            colormap(gray)
            imagesc(this.image);
            axis off
            box off
            for sc=1:numel(this.seeds)
                fseed = this.seeds(sc);
                if fseed.skipped==0
                    memPixels = fseed.membranePixelListLindx;
                    [perListRows,perListCols] = ind2sub(size(this.image),memPixels);
                    hold on
                    plot(perListCols,perListRows,'.','color',0.2 +0.7*rand(1,3),'MarkerSize',10)
                    axis off
                end
            end
            locF2=this.fileSet.resultsDir;
            fnc = [locF2 'finalMemImage'];
            print(offScreenFig6,'-depsc','-r300',fnc);
            %------ end plot membrane Image
            
            fprintf(' Finished cell segmentation\n');
            t=toc;
            fprintf(' Elapsed time: %4.2fs\n', t);
        end
    end
        
   methods (Access=protected)   
        
        function computeHoughTransform(this)
            tic;
            fprintf('   Circular Hough transform\n');
            try
                
                if(this.config.isHoughTransformOnCLAHE)
                    [this.seeds]= CircularHough_Grd(this.imageCLAHE, ...
                        this.config.seedRadiusLimit, ...
                        this.config.houghTransformGradientThreshold, ...
                        this.config.getAccumulationSmoothingWindowSize(), ...
                        1, ...
                        this.config.getHoughTransformAccumulationArraySmoothingFilter, ...
                        this.config.seedSensitivity,...
                        this.config.maximumNumberOfCentroids);
                else
                    [this.seeds] = CircularHough_Grd(this.image, ...
                        this.config.seedRadiusLimit, ...
                        this.config.houghTransformGradientThreshold, ...
                        this.config.getAccumulationSmoothingWindowSize(), ...
                        1, ...
                        this.config.getHoughTransformAccumulationArraySmoothingFilter, ...
                        this.config.seedSensitivity, ...
                        this.config.maximumNumberOfCentroids);
                end
                
            catch exc
                rethrow(exc);
            end
            
            fprintf('    Found %d seed(s)\n', numel(this.seeds));
            t=toc;
            fprintf('   Elapsed time: %4.2fs\n', t);
            
            %---------initial image figure
            offScreenFig0 = figure('visible','off');
            colormap(gray)
            imagesc(this.image);
            axis off
            box off
            locF2=this.fileSet.resultsDir;
            fnc = [locF2 'initialImage'];
            print(offScreenFig0,'-depsc','-r300',fnc);
                        
            %-------plot the inset
            offScreenFig2 = figure('visible','off');
            plot(this.config.membraneIntensityProfile,'r','LineWidth',8)
            axis off
            box off
            fnc = [locF2 'membraneInset'];
            print(offScreenFig2,'-depsc','-r300',fnc);                     
        end
        
        
        function detectMembranes(this)
            seedCount = numel(this.seeds);
            for i = 1:seedCount
                fprintf('   Processing seed %d\n', i);
                this.currentSeed = this.seeds(i);
                this.initCurrentCrops();
                this.membraneDetector.run(...
                    this.currentSeed, ...
                    this.currentCropImage, ...
                    this.currentCropGradientPhase, ...
                    i);
                            
                %---------plot the seed convolution image
                offScreenFig = figure('visible','off');
                imagesc(this.membraneDetector.convolutionImage)
                colorbar('FontSize',18,'location','South');
                caxis([-0.8 1.35])
                axis image
                axis off
                hold on
                Xc=this.membraneDetector.seed.cropCenterX;
                Yc=this.membraneDetector.seed.cropCenterY;
                text(Xc,Yc,['\fontsize{16}{\color{black}' ['S' num2str(i)] '}' ],...
                    'HorizontalAlignment','center','BackgroundColor',[1 1 1],'EdgeColor','black');
                locF2=this.fileSet.resultsDir;
                fnc = [locF2 'convolutionImage_Seed_' num2str(i)];
                print(offScreenFig,'-depsc','-r300',fnc);
                
%                 %----------plot the labeling mask
%                 offScreenFig3 = figure('visible','off');
%                 imagesc(this.membraneDetector.mask)
%                 hold on
%                 text(Xc,Yc,['\fontsize{16}{\color{black}' ['S' num2str(i)] '}' ],...
%                     'HorizontalAlignment','center','BackgroundColor',[1 1 1],'EdgeColor','black');
%                 axis image
%                 axis off
%                 fnc = [locF2 'labelImage_Seed_' num2str(i)];
%                 print(offScreenFig3,'-depsc','-r300',fnc);
                
                if( this.config.debugLevel>1 )
                    disp(this.seeds(i));
                end
            end
            
            
        end
        
        
    end
    
end

