clear; clc; close all;

dbstop if error

thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);
addpath('/home/sotirisd/Desktop/PROJECTS/CellX/core/mscripts');
addpath('/home/sotirisd/Desktop/PROJECTS/CellX/core/mclasses');
addpath('/home/sotirisd/Desktop/PROJECTS/CellX/core/mfunctions');
addpath('/home/sotirisd/Desktop/PROJECTS/CellX/core/mex');
addpath('/home/sotirisd/Desktop/PROJECTS/CellX/core/mex/maxflow');

% define case
scase=312;

%------workflow (cropped region)
if scase==311
    segImage = '../case311/BF_position040511_time0001.tif';
elseif scase==312
     segImage = '../case312/phase_04.tif';
elseif scase==313
     segImage = '../case313/ph2_position010100_time0001.tif';
else
    error('Unknown Case!')
end
    
calibrationfilename = ['./case' num2str(scase) '.xml'];
resultsDir = ['./case'  num2str(scase)];
frameNumber = 1;

    
%--------- produce paper figures
%-------- run segmentation
% get calibration file
config = CellXConfiguration.readXML(calibrationfilename);
config.setDebugLevel(0);
config.check();
% get file set
fileSet = CellXFileSet(frameNumber, segImage);
fileSet.setResultsDirectory(resultsDir);

% run the segmenter
seg = CellXSegmenterFig2patterns(config, fileSet);
seg.run();

segmentedCells =seg.getDetectedCells();
    
    
%     CellXPaperFigureWriter.writeInitialInputImage(...
%         fileSet.oofImage, ...
%         [fileSet.resultsDir 'initialInputImage.png'], ...
%         segmentedCells, ...
%         config...
%         );
%     
%     CellXPaperFigureWriter.writeSeedingControlImage( ...
%         fileSet.oofImage, ...
%         [fileSet.resultsDir 'seedingImage.png'], ...
%         seg.seeds, ...
%         config...
%         );
%     
%     CellXPaperFigureWriter.writeSegmentationControlImageWithIndicesB(...
%         fileSet.oofImage, ...
%         [fileSet.resultsDir 'segmentationImage.eps'], ...
%         segmentedCells, ...
%         config...
%         );
    

