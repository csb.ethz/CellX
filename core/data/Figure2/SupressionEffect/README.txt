% case of Membrane Pattern Phase Contrast 
% for image for Paper Figure 3
if ccase==331
    settings = getSettings();
    settings.UseInit = 1;
    settings.UseAdaptForGC = 0 ;
    settings.NonConstantPattern = 1 ;
    settings.FigureNumber =2;
    settings.debug =1;
    settings.radMat = [25 35];
    settings.ElongationLength = 150;
    settings.CropInteract = 0;
    settings.CropRegion = [469 481  473  283];
    settings.IsNotConvex = 0;
    settings.ExpansionThresh = 1; 
    settings.MembraneSignal = [0.4, 0.45 , 0.55 ,0.65 , 0.7, 0.75, 0.8, ones(1,5),...
                                0.95, 0.85 , 0.8 , 0.7 ,0.65, 0.55 ];
    settings.MembraneSignalLocation = 8;
    settings.MembraneWidth = 6;
    settings.SeedSensitivity  = 0.3; 
    
    settings.bf_out_of_focus_image = [loc 'Figure3_crowded/MembranePattern/PhaseContrast/phase_03.tif'];
    settings.bf_in_focus_image     = [loc 'Figure3_crowded/MembranePattern/PhaseContrast/phase_03.tif'];
    settings.seg_image_out = '../ResultFolder/seg_test_mpattern_phcon.png';
    settings.results_file  = '../ResultFolder/results_mpattern_phcon.txt';
    settings.frameno =  1;

   checkSettings(settings);
   main(settings);
end

